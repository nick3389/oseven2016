# OSeven2016

[![CI Status](http://img.shields.io/travis/Nick Xirotyris/OSeven2016.svg?style=flat)](https://travis-ci.org/Nick Xirotyris/OSeven2016)
[![Version](https://img.shields.io/cocoapods/v/OSeven2016.svg?style=flat)](http://cocoapods.org/pods/OSeven2016)
[![License](https://img.shields.io/cocoapods/l/OSeven2016.svg?style=flat)](http://cocoapods.org/pods/OSeven2016)
[![Platform](https://img.shields.io/cocoapods/p/OSeven2016.svg?style=flat)](http://cocoapods.org/pods/OSeven2016)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

OSeven2016 is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "OSeven2016"
```

## Author

Nick Xirotyris, nick.xirotyris@gmail.com

## License

OSeven2016 is available under the MIT license. See the LICENSE file for more info.
