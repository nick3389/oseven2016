//
//  DemoStruct.swift
//  Pods
//
//  Created by Nick Xirotyris on 27/10/2016.
//
//

import Foundation

public struct DemoStruct {
    public var test = 1
    public var best = true
    
    public init() {
    }
}
